export default {
  external: ['react', 'react-dom'],
  input: 'output/esm/main.js',
  output: {
    file: 'output/build/main.js',
    format: 'iife',
    globals: {
      react: 'React',
      'react-dom': 'ReactDOM'
    }
  },
  watch: {
    clearScreen: false
  }
}
