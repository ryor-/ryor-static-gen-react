'use strict'

const VENDOR_SCRIPT_PATHS = [
  'react/umd/react.development.js',
  'react-dom/umd/react-dom.development.js'
]

module.exports = {
  description: 'Run initial development build of site and then starts file watchers and Browsersync server',
  run: () => [
    'shx rm -rf output',
    'shx mkdir -p output/build output/esm',
    [
      'node run html -dq',
      `shx cp -r ${VENDOR_SCRIPT_PATHS.map(path => `node_modules/${path}`).join(' ')} output/build`,
      'shx touch output/esm/main.js'
    ],
    `concurrently -c blue,green,magenta,cyan -n "[Sass       ],[TypeScript ],[Rollup     ]," -p {name} ${[
      'node run sass -dw',
      'node run tsc -w',
      'node run rollup -dw',
      'browser-sync output/build --no-ghost-mode --no-notify --no-open -w'
    ].map(script => `"${script}"`).join(' ')}`
  ]
}
